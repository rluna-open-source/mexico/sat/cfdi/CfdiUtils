<?php

namespace CfdiUtils\Elements\CartaPorte10;

use CfdiUtils\Elements\Common\AbstractElement;

class Notificado extends AbstractElement
{
    public function getElementName(): string
    {
        return 'cartaporte:Notificado';
    }
}
